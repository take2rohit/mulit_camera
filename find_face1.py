import face_recognition
import cv2
import numpy as np
# Library for csv file
import csv

# Preparing the csv file for the writing purpose
csv_file = open("Details1.csv","w")
csv_writer = csv.writer(csv_file)
csv_writer.writerow(["Camera_no.","Time"])

cv2.namedWindow('1', cv2.WINDOW_NORMAL)

a = cv2.imread('r.png')
def find_face(img,seq):

	sequence = str(seq)
	
	video_capture = cv2.VideoCapture('/home/rohit/test/multicamera_tracking_modified_one/out1.avi')
	# video_capture.open('rtsp://admin:Rajini45@192.168.0.250:554/streaming/channels/1')
	# video_capture.open('rtsp://admin:Loksun@69@192.168.0.7:554/1')
	# Load a sample picture and learn how to recognize it.
	
	obama_face_encoding = face_recognition.face_encodings(img)[0]

	# Load a second sample picture and learn how to recognize it.

	# Create arrays of known face encodings and their names
	known_face_encodings = [
		obama_face_encoding
	]
	known_face_names = [
		"Tracked"
	]

	# Initialize some variables
	face_locations = []
	face_encodings = []
	face_names = []
	process_this_frame = True
	scale = 0.25
	count = 0
	while True:
		try:
			ret, frame = video_capture.read()

			# Resize frame of video to 1/4 size for faster face recognition processing
			small_frame = cv2.resize(frame, (0, 0), fx=scale, fy=scale)

			# Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
			rgb_small_frame = small_frame[:, :, ::-1]
			count+=1
			# Only process every other frame of video to save time
			if process_this_frame and count%5 == 0:
				# Find all the faces and face encodings in the current frame of video
				face_locations = face_recognition.face_locations(rgb_small_frame)
				face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)

				face_names = []
				for face_encoding in face_encodings:
					# See if the face is a match for the known face(s)
					matches = face_recognition.compare_faces(known_face_encodings, face_encoding)
					name = "Unknown"

					# # If a match was found in known_face_encodings, just use the first one.
					# if True in matches:
					#     first_match_index = matches.index(True)
					#     name = known_face_names[first_match_index]

					# Or instead, use the known face with the smallest distance to the new face
					face_distances = face_recognition.face_distance(known_face_encodings, face_encoding)
					best_match_index = np.argmin(face_distances)
					if matches[best_match_index]:
						name = known_face_names[best_match_index]

					face_names.append(name)

			process_this_frame = not process_this_frame

			if count%1 == 0:
			# Display the results
				for (top, right, bottom, left), name in zip(face_locations, face_names):
					# Scale back up face locations since the frame we detected in was scaled to 1/4 size
					top *= int(1/scale)
					right *= int(1/scale)
					bottom *= int(1/scale)
					left *= int(1/scale)
					print("top {}".format(top))
					print("right{}".format(right))
					print("left {}".format(left))
					print(" bottom{}".format(bottom))
			
			
					# Draw a box around the face
					cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

					# Draw a label with a name below the face
					cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
					font = cv2.FONT_HERSHEY_DUPLEX
					cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)
				# modify this line for different cameras
				# run this file for multiple videos
					csv_writer.writerow(["Camera_1",str(np.datetime64("now"))])  
				# Display the resulting image
			cv2.imshow(sequence, frame)

			# Hit 'q' on the keyboard to quit!
			if cv2.waitKey(1) & 0xFF == ord('q'):
				break
		
		except Exception as e:
			csv_file.close()
			video_capture.release()
			cv2.destroyAllWindows()
			print('over')
			break

find_face(a,1)
