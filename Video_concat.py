import cv2
import numpy as np
import os
import argparse
import sys

#ap = argparse.ArgumentParser()
#ap.add_argument("-n","--number",help ="Number of vidoes")
#num  = vars(ap.parse_args())
def four_vidoes(vid_1,vid_2,vid_3,vid_4,out_1):
	cap_1 = cv2.VideoCapture(os.path.join(os.getcwd(),vid_1),0)
	cap_2 = cv2.VideoCapture(os.path.join(os.getcwd(),vid_2),0)
	cap_3 = cv2.VideoCapture(os.path.join(os.getcwd(),vid_3),0)
	cap_4 = cv2.VideoCapture(os.path.join(os.getcwd(),vid_4),0)

	fourcc= cv2.VideoWriter_fourcc(*"XVID")	
	out = cv2.VideoWriter(out_1,fourcc,20.0,(1920//2,1080//2))

	#out = cv2.VideoWriter("video.avi",cv2.cv.CV_FOURCC('F','M','P', '4'),15, (1200, 600), 1)	
	#cv2.VideoWriter_fourcc(*args["codec"])
	while(cap_1.isOpened()):
		ret_1,frame_1 = cap_1.read()
		ret_2,frame_2 = cap_2.read()
		ret_3,frame_3 = cap_3.read()
		ret_4,frame_4 = cap_4.read()
		if(ret_1 and ret_2 and ret_3 and ret_4):
			frame_1 = cv2.resize(frame_1,(1920//4,1080//4))
			frame_2 = cv2.resize(frame_2,(1920//4,1080//4))
			frame_3 = cv2.resize(frame_3,(1920//4,1080//4))
			frame_4 = cv2.resize(frame_4,(1920//4,1080//4))
			both_1  = np.concatenate((frame_1,frame_2),axis=1)
			both_2  = np.concatenate((frame_3,frame_4),axis=1)
			final   = np.concatenate((both_1,both_2),axis=0)
			out.write(final)
			if(cv2.waitKey(0) & 0XFF ==ord("q")):
				sys.exit()
		else:
			print("!!COULD NOT READ")
			break
	
	cap_1.release()
	cap_2.release()
	cap_3.release()
	cap_4.release()
def eight_vidoes(vid_1,vid_2,vid_3,vid_4,vid_5,vid_6,vid_7,vid_8,out_1):
	print("caps region")
	cap_1 = cv2.VideoCapture(os.path.join(os.getcwd(),vid_1),0)
	cap_2 = cv2.VideoCapture(os.path.join(os.getcwd(),vid_2),0)
	cap_3 = cv2.VideoCapture(os.path.join(os.getcwd(),vid_3),0)
	cap_4 = cv2.VideoCapture(os.path.join(os.getcwd(),vid_4),0)
	cap_5 = cv2.VideoCapture(os.path.join(os.getcwd(),vid_5),0)
	cap_6 = cv2.VideoCapture(os.path.join(os.getcwd(),vid_6),0)
	cap_7 = cv2.VideoCapture(os.path.join(os.getcwd(),vid_7),0)
	cap_8 = cv2.VideoCapture(os.path.join(os.getcwd(),vid_8),0)

	fourcc= cv2.VideoWriter_fourcc(*"XVID")	
	out = cv2.VideoWriter(out_1,fourcc,20.0,(800,800))

	#out = cv2.VideoWriter("video.avi",cv2.cv.CV_FOURCC('F','M','P', '4'),15, (1200, 600), 1)	
	#cv2.VideoWriter_fourcc(*args["codec"])
	print("Going while")
	while(cap_1.isOpened()):
		print("Inside while")
		ret_1,frame_1 = cap_1.read()
		ret_2,frame_2 = cap_2.read()
		ret_3,frame_3 = cap_3.read()
		ret_4,frame_4 = cap_4.read()
		ret_5,frame_5 = cap_5.read()
		ret_6,frame_6 = cap_6.read()
		ret_7,frame_7 = cap_7.read()
		ret_8,frame_8 = cap_8.read()
		print("Going in if")
		if(ret_1 and ret_2 and ret_3 and ret_4 and ret_5 and ret_6 and ret_7 and ret_8):
			frame_1 = cv2.resize(frame_1,(200,200))
			frame_2 = cv2.resize(frame_2,(200,200))
			frame_3 = cv2.resize(frame_3,(200,200))
			frame_4 = cv2.resize(frame_4,(200,200))
			frame_5 = cv2.resize(frame_5,(200,200))
			frame_6 = cv2.resize(frame_6,(200,200))
			frame_7 = cv2.resize(frame_7,(200,200))
			frame_8 = cv2.resize(frame_8,(200,200))
			both_1  = np.concatenate((frame_1,frame_2),axis=1)
			both_2  = np.concatenate((frame_3,frame_4),axis=1)
			both_3  = np.concatenate((frame_5,frame_6),axis=1)
			both_4  = np.concatenate((frame_7,frame_8),axis=1)
	
			final_1 = np.concatenate((both_1,both_2),axis=1)
			final_2 = np.concatenate((both_3,both_4),axis=1)
			final = np.concatenate((final_1,final_2),axis=0)
			
			out.write(final)
			if(cv2.waitKey(0) & 0XFF ==ord("q")):
				sys.exit()
		else:
			print("!!COULD NOT READ")
			break
	
	cap_1.release()
	cap_2.release()
	cap_3.release()
	cap_4.release()
	cap_5.release()
	cap_6.release()
	cap_7.release()
	cap_8.release()


print("[INFO]!! Reading the video.......")
four_vidoes('2.mp4','2.mp4','3.mp4','4.mp4','out1.avi')

cv2.destroyAllWindows()
